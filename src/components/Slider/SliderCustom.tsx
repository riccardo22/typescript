import React from "react";
import SliderItem from "./SliderItem";
import Typography from "@mui/material/Typography";
import _ from "lodash";

interface SliderCustomProps {
  title: string;
  data: Array<{ id: number; name: string; img: string }>;
}

const SliderCustom: React.FC<SliderCustomProps> = ({ title, data }) => {
  return (
    <div className={"my-12"}>
      <Typography variant={"h4"} sx={{ ml: 2 }}>
        {title}
      </Typography>
      <div className="relative w-full flex gap-6 overflow-x-auto pb-8 px-14 pt-6 snap-x">
        {data.map((res, index) => {
          return <SliderItem img={res.img} index={index} />;
        })}
      </div>
    </div>
  );
};

export default SliderCustom;
