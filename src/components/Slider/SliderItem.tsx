import React, { useState } from "react";
import { Button } from "@mui/material";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import Typography from "@mui/material/Typography";

interface SliderItemProps {
  index: number;
  img: string;
}

const SliderItem: React.FC<SliderItemProps> = ({ index, img }) => {
  const [hover, setHover] = useState<boolean>(false);
  return (
    <div
      key={index}
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      className="snap-normal snap-center shrink-0 cursor-pointer"
    >
      {hover ? (
        <>
          <div className={" flex-col"}>
            <div className={"brightness-50 relative"}>
              <img
                style={{
                  transition: "transform .1s",
                  transform: "scale(1.2)",
                }}
                alt="img"
                src={img}
              />
            </div>
            <div className={"relative inset-x-0 "}>
              <Button
                variant="contained"
                fullWidth
                sx={{ position: "absolute", bottom: 0 }}
                size={"small"}
                endIcon={<AddShoppingCartIcon />}
                // onClick={() => setOpen(true)}
              >
                AGGIUNGI AL CARRELLO
              </Button>
            </div>
          </div>
        </>
      ) : (
        <img style={{ transition: "transform .1s" }} alt="img" src={img} />
      )}
    </div>
  );
};

export default SliderItem;
