import * as React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import { ListCustomProps } from "../../interfaces/interfaces";

const ListCustom: React.FC<ListCustomProps> = ({
  id,
  name,
  lastname,
  email,
}) => {
  return (
    <List
      key={id}
      sx={{
        width: "100%",
        maxWidth: 360,
        backgroundColor: "background.paper",
        margin: 2,
      }}
    >
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar
            alt="Remy Sharp"
            src={
              "https://images.unsplash.com/profile-1446404465118-3a53b909cc82?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=64&w=64&s=3ef46b07bb19f68322d027cb8f9ac99f"
            }
          />
        </ListItemAvatar>
        <ListItemText
          primary={name}
          secondary={
            <React.Fragment>
              <Typography
                sx={{ display: "inline" }}
                component="span"
                variant="body2"
                color="text.primary"
              >
                {lastname}
              </Typography>
              {email}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider className={"w-96"} variant="inset" component="li" />
    </List>
  );
};
export default ListCustom;
