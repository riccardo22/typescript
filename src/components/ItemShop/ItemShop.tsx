import React, { useState } from "react";
import { Button } from "@mui/material";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import { ProductsProps } from "../../Screen/Home/home";
import DialogCustom from "../Dialog/DialogCustom";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch, RootState } from "../../rematch/store";

const ItemShop: React.FC<ProductsProps> = ({ img, id }) => {
  const [open, setOpen] = useState<boolean>(false);
  const [hover, setHover] = useState<boolean>(false);
  const count = useSelector((state: RootState) => state.count);
  const dispatch = useDispatch<Dispatch>();
  return (
    <div
      className={"cursor-pointer"}
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      key={id}
    >
      {!hover ? (
        <div className={"my-4"}>
          <img alt="img" src={img} />
        </div>
      ) : (
        <div className={"my-4 relative"}>
          <img className={"brightness-50 "} alt="img" src={img} />
          <Button
            variant={"contained"}
            sx={{
              position: "absolute",
              bottom: 20,
              borderRadius: 0,
            }}
            fullWidth
            size={"small"}
            endIcon={<AddShoppingCartIcon />}
            // onClick={() => setOpen(true)}
          >
            AGGIUNGI AL CARRELLO
          </Button>
        </div>
      )}

      <DialogCustom
        title={"Attenzione!"}
        open={open}
        setOpen={setOpen}
        content={"Sicuro di voler aggiungerlo al carrello?"}
        titleButton={"AGGIUNGI"}
        colorButton={"primary"}
        onClickButton={() => {
          dispatch.count.setCart(count.cart + 1);
          setOpen(false);
        }}
      />
    </div>
  );
};

export default ItemShop;
