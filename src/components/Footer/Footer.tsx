import React from "react";
import Box from "@mui/material/Box";
import logo from "../../assets/img/logo.png";
import payment from "../../assets/img/payment.png";
import Typography from "@mui/material/Typography";
import GoogleIcon from "@mui/icons-material/Google";
import TwitterIcon from "@mui/icons-material/Twitter";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import { TextField } from "@mui/material";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";

const Footer = () => {
  return (
    <>
      <Box
        sx={{
          gridArea: "footer",
          backgroundColor: "#f9f9f9",
          height: "100%",
          marginTop: 10,
          paddingBottom: 15,
          paddingTop: 5,
        }}
      >
        <div className={"grid grid-cols-3 mb-12 mt-12 mx-44"}>
          <div className={"grid grid-rows-2"}>
            <Typography fontWeight={"bold"} variant={"h5"}>
              CONOSCI TUTTO SUBITO!
            </Typography>
            <div>
              Never Miss Anything From Multikart By Signing Up To Our
              Newsletter.
            </div>
          </div>
          <div className={"flex justify-center"}>
            <Divider orientation="vertical" />
          </div>
          <div className={"grid grid-cols-3 my-auto"}>
            <TextField
              className={"col-span-2 "}
              name={"name"}
              label={"Email"}
              placeholder={"Inserisci la mail"}
              type={"email"}
              fullWidth
            />
            <div className={"ml-6 my-auto"}>
              <Button fullWidth size={"large"} variant={"contained"}>
                INVIA
              </Button>
            </div>
          </div>
        </div>
        <div className={"mx-44"}>
          <Divider variant="fullWidth" />
        </div>
        <div
          className={
            "grid grid-cols-4 text-center h-72 justify-items-center mx-44 mt-12 h-full"
          }
        >
          <div className={"grid grid-rows-3 justify-items-center"}>
            <img src={logo} alt={"logo"} />
            <Typography align={"left"} variant={"subtitle1"}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam.
            </Typography>
            <div className={"grid grid-cols-4 w-full items-center"}>
              <div>
                <GoogleIcon />
              </div>
              <div>
                <TwitterIcon />
              </div>
              <div>
                <FacebookIcon />
              </div>
              <div>
                <InstagramIcon />
              </div>
            </div>
          </div>
          <div className={"grid grid-rows-5 justify-items-center w-full"}>
            <Typography fontWeight={"bold"} variant={"h5"}>
              MY ACCOUNT
            </Typography>
            <div>Womens</div>
            <div>Clothing</div>
            <div>Accessories</div>
            <div>Featured</div>
          </div>
          <div className={"grid grid-rows-6 justify-items-center w-full"}>
            <Typography fontWeight={"bold"} variant={"h5"}>
              CHI SIAMO
            </Typography>
            <div>Shipping & Return</div>
            <div>Secure shopping</div>
            <div>Gallary</div>
            <div>Affiliates</div>
            <div>Contacts</div>
          </div>
          <div
            className={"grid grid-rows-5 justify-items-center w-full h-full"}
          >
            <Typography fontWeight={"bold"} variant={"h5"}>
              INFOMRAZIONI
            </Typography>
            <div>Multi Kart Demo Store</div>
            <div>Call: 123-456-7890</div>
            <div>Email: kart@gmail.com</div>
            <div>Fax: 123456</div>
          </div>
        </div>
      </Box>
      <Box sx={{ marginY: 2 }}>
        <div className={"grid grid-cols-2"}>
          <p className={"ml-12"}>2020-21 themeforest powered by pixelstrap</p>
          <div className={"flex justify-end mr-24 gap-4"}>
            <img src={payment} alt={"payment"} />
            <img src={payment} alt={"payment"} />
            <img src={payment} alt={"payment"} />
            <img src={payment} alt={"payment"} />
          </div>
        </div>
      </Box>
    </>
  );
};

export default Footer;
