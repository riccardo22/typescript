import * as React from "react";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { useDispatch } from "react-redux";
import { Dispatch } from "../../rematch/store";
import { useNavigate } from "react-router-dom";
import Avatar from "@mui/material/Avatar";
import { NavCustomProps } from "../NavBar/NavCustom";
import { IconButton, ListItemIcon } from "@mui/material";
import Divider from "@mui/material/Divider";
import { Logout, PersonAdd, Settings } from "@mui/icons-material";
import { makeStyles } from "@mui/styles";

interface MenuCustomProps {
  avatar: NavCustomProps["avatar"];
}

const MenuCustom: React.FC<MenuCustomProps> = ({ avatar }) => {
  const dispatch = useDispatch<Dispatch>();
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <IconButton
        aria-owns={anchorEl ? "simple-menu" : undefined}
        aria-haspopup="true"
        sx={{ marginX: 2 }}
        // onMouseOver={handleClick}
        onClick={handleClick}
      >
        <Avatar sx={{ backgroundColor: "red" }}>{avatar}</Avatar>
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: style,
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem>
          <Avatar /> Profile
        </MenuItem>
        <MenuItem>
          <Avatar /> My account
        </MenuItem>
        <Divider />
        <MenuItem onClick={() => navigate("/register")}>
          <ListItemIcon>
            <PersonAdd fontSize="small" />
          </ListItemIcon>
          Aggiungi nuovo account
        </MenuItem>
        <MenuItem>
          <ListItemIcon>
            <Settings fontSize="small" />
          </ListItemIcon>
          Impostazioni
        </MenuItem>
        <MenuItem
          onClick={() => {
            dispatch.count.setToken(0);
            navigate("/");
          }}
        >
          <ListItemIcon>
            <Logout fontSize="small" />
          </ListItemIcon>
          Esci
        </MenuItem>
      </Menu>
    </>
  );
};
export default MenuCustom;

const style = {
  overflow: "visible",
  filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
  mt: 1.5,
  "& .MuiAvatar-root": {
    width: 32,
    height: 32,
    ml: -0.5,
    mr: 1,
  },
  "&:before": {
    content: '""',
    display: "block",
    position: "absolute",
    top: 0,
    right: 14,
    width: 10,
    height: 10,
    bgcolor: "background.paper",
    transform: "translateY(-50%) rotate(45deg)",
    zIndex: 0,
  },
};
