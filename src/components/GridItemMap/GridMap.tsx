import React from "react";
import ItemShop from "../ItemShop/ItemShop";
import PaginationCustom from "../Pagination/PaginationCustom";
import { ProductsProps } from "../../Screen/Home/home";
import Typography from "@mui/material/Typography";

interface GridMapProps {
  data: Array<{ id: number; img: string }>;
  pageTotal: number;
  setPage: (n: number) => void;
  title: string;
}

const GridMap: React.FC<GridMapProps> = ({
  data,
  pageTotal,
  setPage,
  title,
}) => {
  return (
    <>
      <Typography variant={"h4"}>{title}</Typography>
      <div className={"grid grid-cols-8 justify-items-center mx-4 mt-24"}>
        {data.map((res: ProductsProps) => (
          <ItemShop id={res.id} img={res.img} />
        ))}
      </div>
      <PaginationCustom pageTotal={pageTotal} setPage={setPage} />
    </>
  );
};

export default GridMap;
