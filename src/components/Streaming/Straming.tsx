import React from "react";

interface StreamingProps {
  key: number;
  img: string;
  onClickCustom: () => void;
}

const Streaming: React.FC<StreamingProps> = ({ img, onClickCustom, key }) => {
  return (
    <div key={key}>
      <img
        className={
          "rounded-lg w-12  cursor-pointer border border-gray-200 hover:brightness-50"
        }
        onClick={onClickCustom}
        src={img}
        alt={"img"}
      />
    </div>
  );
};

export default Streaming;
