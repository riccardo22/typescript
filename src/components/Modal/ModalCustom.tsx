import * as React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "30%",
  borderRadius: 3,
  bgcolor: "background.paper",
  boxShadow: 24,
  maxHeight: "60%",
  overflow: "scroll",
  p: 4,
};

interface ModalCustomProps {
  open: boolean;
  setOpen: (b: boolean) => void;
  children: React.ReactNode;
}

const ModalCustom: React.FC<ModalCustomProps> = ({
  open,
  setOpen,
  children,
}) => {
  return (
    <div>
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>{children}</Box>
      </Modal>
    </div>
  );
};

export default ModalCustom;
