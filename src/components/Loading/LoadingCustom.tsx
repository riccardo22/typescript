import React from "react";
import { Box, CircularProgress } from "@mui/material";

const LoadingCustom = () => {
  return (
    <Box
      sx={{
        display: "flex",
        height: "100vh",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <CircularProgress />
    </Box>
  );
};

export default LoadingCustom;
