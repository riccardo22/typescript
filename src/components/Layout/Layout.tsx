import * as React from "react";
import NavCustom from "../../components/NavBar/NavCustom";
import { useSelector } from "react-redux";
import { RootState } from "../../rematch/store";
import { memo } from "react";
import Footer from "../../components/Footer/Footer";
import logo from "../../assets/img/logo.png";

const link = [
  { id: 0, name: "home", to: "/home" },
  { id: 1, name: "page1", to: "/page1" },
  { id: 2, name: "page2", to: "/page2" },
  { id: 3, name: "page3", to: "/page3" },
];

interface HomeProps {
  children?: React.ReactNode;
}

const Layout: React.FC<HomeProps> = ({ children }) => {
  const count = useSelector((state: RootState) => state.count);
  return (
    <>
      <NavCustom
        logo={logo}
        link={link}
        avatar={count.bedgeLetter}
        name={count.name}
        lastname={count.lastname}
      />
      {children}
      <>
        <Footer />
      </>
    </>
  );
};

export default memo(Layout);
