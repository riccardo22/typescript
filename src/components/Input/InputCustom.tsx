import React from "react";
import { Controller, useForm } from "react-hook-form";
import { TextField } from "@mui/material";
import { yupResolver } from "@hookform/resolvers/yup";

interface InputCustomProps {
  label: string;
  name: string;
}

const InputCustom: React.FC<InputCustomProps> = ({ label, name }) => {
  const {
    control,
    formState: { errors },
  } = useForm();

  return <></>;
};

export default InputCustom;
