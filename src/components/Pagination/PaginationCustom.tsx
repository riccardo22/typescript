import React from "react";
import Divider from "@mui/material/Divider";
import { Pagination } from "@mui/material";

interface PaginationCustomProps {
  pageTotal: number;
  setPage: (n: number) => void;
}

const PaginationCustom: React.FC<PaginationCustomProps> = ({
  pageTotal,
  setPage,
}) => {
  const handleChange = (page: number) => {
    setPage(page);
    window.scroll(0, 0);
  };

  return (
    <>
      <Divider variant={"middle"} sx={{ marginTop: 8, marginBottom: 2 }} />
      <div className={"flex justify-end"}>
        <Pagination
          count={pageTotal}
          color="primary"
          onChange={(e, page) => handleChange(page)}
        />
      </div>
    </>
  );
};

export default PaginationCustom;
