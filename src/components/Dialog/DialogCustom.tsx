import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { OverridableStringUnion } from "@mui/types";
import { ButtonPropsColorOverrides } from "@mui/material/Button/Button";

interface DialogCustomProps {
  open: boolean;
  setOpen: (b: boolean) => void;
  title?: string;
  content: string;
  titleButton: string;
  onClickButton: () => void;
  colorButton:
    | "inherit"
    | "primary"
    | "secondary"
    | "success"
    | "error"
    | "info"
    | "warning";
}

const DialogCustom: React.FC<DialogCustomProps> = ({
  open,
  setOpen,
  title,
  content,
  titleButton,
  onClickButton,
  colorButton,
}) => {
  return (
    <div>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {content}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant={"outlined"} onClick={() => setOpen(false)}>
            CHIUDI
          </Button>
          <Button
            variant={"contained"}
            color={colorButton}
            onClick={onClickButton}
            autoFocus
          >
            {titleButton}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DialogCustom;
