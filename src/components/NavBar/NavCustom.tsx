import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import MenuCustom from "../Menu/MenuCustom";
import { useState } from "react";
import Badge from "@mui/material/Badge";
import IconButton from "@mui/material/IconButton";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { useSelector } from "react-redux";
import { RootState } from "../../rematch/store";
import DrawerCustom from "../Drawer/DrawerCustom";

export interface NavCustomProps {
  logo: string;
  link: Array<{ id: number; name: string; to: string }>;
  avatar: string;
  name: string;
  lastname: string;
}

const NavCustom: React.FC<NavCustomProps> = ({
  logo,
  link,
  avatar,
  name,
  lastname,
}) => {
  const navigate = useNavigate();
  const count = useSelector((state: RootState) => state.count);
  const [openMenu, setOpenMenu] = useState(false);
  return (
    <Box component={"div"} sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <DrawerCustom />
          <Box
            sx={{
              flexGrow: 1,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              cursor: "pointer",
            }}
          >
            <img alt={"logo"} src={logo} />
          </Box>

          {link.map((res) => (
            <>
              <Button
                sx={{ marginX: 4, fontSize: 15 }}
                key={res.id}
                onMouseEnter={() => setOpenMenu(true)}
                onClick={() => navigate(res.to)}
                color="inherit"
              >
                {res.name}
              </Button>
            </>
          ))}
          <Typography
            sx={{
              marginLeft: 2,
              fontSize: 17,
              fontWeight: "bold",
            }}
            variant={"h6"}
            component={"h6"}
          >
            {name} {lastname}
          </Typography>
          <MenuCustom avatar={avatar} />
          <IconButton aria-label="cart">
            {count.cart !== 0 ? (
              <Badge
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "right",
                }}
                badgeContent={count.cart}
                color="error"
              >
                <ShoppingCartIcon className={"text-white"} />
              </Badge>
            ) : (
              <ShoppingCartIcon className={"text-white"} />
            )}
          </IconButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default NavCustom;
