import * as React from "react";
import { makeStyles } from "@mui/styles";
import { useGridApiContext, useGridState } from "@mui/x-data-grid";
import Pagination from "@mui/material/Pagination";

const useStyles = makeStyles({
  root: {
    display: "flex",
    marginTop: 5,
  },
});

export const PaginationCustom = () => {
  // const { state, apiRef } = useGridSlotComponentProps();
  const classes = useStyles();
  const apiRef = useGridApiContext();
  const [state] = useGridState(apiRef);

  return (
    <Pagination
      className={classes.root}
      color="primary"
      count={state.pagination.pageCount}
      page={state.pagination.page + 1}
      onChange={(event, value) => apiRef.current.setPage(value - 1)}
    />
  );
};
