import * as React from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

interface BottomNavigationCustomProps {
  children1: React.ReactNode;
  children2: React.ReactNode;
}

const TabsNavigationCustom: React.FC<BottomNavigationCustomProps> = ({
  children1,
  children2,
}) => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  return (
    <Box sx={{ width: "100%", marginTop: 2 }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab sx={{ width: 200 }} label="Film" />
          <Tab sx={{ width: 200 }} label="Serie TV" />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        {children1}
      </TabPanel>
      <TabPanel value={value} index={1}>
        {children2}
      </TabPanel>
    </Box>
  );
};
export default TabsNavigationCustom;
