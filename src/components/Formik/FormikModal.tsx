import React from "react";
import { useFormik } from "formik";
import { TextField } from "@mui/material";
import Button from "@mui/material/Button";
import * as yup from "yup";
import Typography from "@mui/material/Typography";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch, RootState } from "../../rematch/store";

interface FormikModalProps {
  title: string;
  titleButton: string;
  url: string;
  setOpen: (b: boolean) => void;
  customer?: { id: number; name: string; lastname: string; email: string };
}

const validationSchema = yup
  .object({
    name: yup.string().required("Campo obbligatorio"),
    lastname: yup.string().required("Campo obbligatorio"),
    email: yup
      .string()
      .email("Inserisci una mail valida")
      .required("Campo obbligatorio"),
  })
  .required();

const FormikModal: React.FC<FormikModalProps> = ({
  title,
  titleButton,
  url,
  setOpen,
  customer,
}) => {
  const dispatch = useDispatch<Dispatch>();
  const count = useSelector((state: RootState) => state.count);
  const axiosConfig = {
    headers: {
      Authorization: "Bearer " + count.token,
    },
  };
  const formik = useFormik({
    enableReinitialize: true,
    validateOnChange: true,
    initialValues: {
      id: customer ? customer.id : "",
      name: customer ? customer.name : "",
      lastname: customer ? customer.lastname : "",
      email: customer ? customer.email : "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      axios
        .post(
          url,
          {
            id: values.id,
            user_id: count.idUser,
            name: values.name,
            lastname: values.lastname,
            email: values.email,
          },
          axiosConfig
        )
        .then((res) => {
          if (res.data.success === 1) {
            dispatch.count.setSnackbar(true);
            setOpen(false);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <div className={"flex flex-col"}>
        <Typography sx={{ marginBottom: 3 }} variant={"h5"}>
          {title}
        </Typography>
        <TextField
          sx={{ marginBottom: 3 }}
          name={"name"}
          title={"Nome"}
          placeholder={"Inserisci il nome"}
          defaultValue={formik.values.name}
          error={formik.touched.name && Boolean(formik.errors.name)}
          value={formik.values.name}
          helperText={formik.touched.name && formik.errors.name}
          onChange={formik.handleChange}
        />
        <TextField
          sx={{ marginBottom: 3 }}
          name={"lastname"}
          title={"Cognome"}
          placeholder={"Inserisci il cognome"}
          defaultValue={formik.values.lastname}
          error={formik.touched.lastname && Boolean(formik.errors.lastname)}
          value={formik.values.lastname}
          helperText={formik.touched.lastname && formik.errors.lastname}
          onChange={formik.handleChange}
        />
        <TextField
          sx={{ marginBottom: 6 }}
          type={"email"}
          name={"email"}
          title={"Email"}
          placeholder={"Inserisci la email"}
          defaultValue={formik.values.email}
          error={formik.touched.email && Boolean(formik.errors.email)}
          value={formik.values.email}
          helperText={formik.touched.email && formik.errors.email}
          onChange={formik.handleChange}
        />
        <Button variant={"contained"} size={"large"} type="submit">
          {titleButton}
        </Button>
      </div>
    </form>
  );
};

export default FormikModal;
