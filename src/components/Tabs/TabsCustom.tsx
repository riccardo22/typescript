import React from "react";
import Box from "@mui/material/Box";
import { Button, Stack } from "@mui/material";
import { useNavigate } from "react-router-dom";

interface TabsCustomProps {
  page1: string;
  page2: string;
  page3: string;
}

const TabsCustom: React.FC<TabsCustomProps> = ({ page1, page2, page3 }) => {
  const navigate = useNavigate();
  return (
    <Box className={"bg-gray-200 p-8 mb-4 shadow"}>
      <Stack
        className={"justify-center"}
        spacing={2}
        direction="row"
        textAlign={"center"}
      >
        <Button onClick={() => navigate(`/${page1}`)} variant="contained">
          PAGE 1
        </Button>
        <Button onClick={() => navigate(`/${page2}`)} variant="contained">
          PAGE 2
        </Button>
        <Button onClick={() => navigate(`/${page3}`)} variant="contained">
          PAGE 3
        </Button>
      </Stack>
    </Box>
  );
};

export default TabsCustom;
