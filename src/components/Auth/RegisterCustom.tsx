import React from "react";
import { Controller, useForm } from "react-hook-form";
import { Button, TextField } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch, RootState } from "../../rematch/store";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import * as yup from "yup";

interface LoginCustomProps {
  url: string;
  route: string;
  setLoading: (b: boolean) => void;
}

interface IFormInputsRegister {
  name: string;
  lastname: string;
  email: string;
  password: string;
  password_confirmation: string;
}

const SignupSchema = yup
  .object({
    name: yup.string().required("Campo obbligatorio"),
    lastname: yup.string().required("Campo obbligatorio"),
    email: yup.string().email().required("Campo obbligatorio"),
    password: yup.string().required("Campo obbligatorio"),
    password_confirmation: yup
      .string()
      .oneOf([yup.ref("password"), null], "Password non corrispondente")
      .required("Campo obbligatorio"),
  })
  .required();

const RegisterCustom: React.FC<LoginCustomProps> = ({
  url,
  route,
  setLoading,
}) => {
  const navigate = useNavigate();
  const count = useSelector((state: RootState) => state.count);
  const dispatch = useDispatch<Dispatch>();
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<IFormInputsRegister>({
    defaultValues: {
      name: "",
      lastname: "",
      email: "",
      password: "",
      password_confirmation: "",
    },
    resolver: yupResolver(SignupSchema),
  });

  const onSubmit = (data: IFormInputsRegister) => {
    setLoading(true);
    axios
      .post(url, {
        name: data.name,
        lastname: data.lastname,
        email: data.email,
        password: data.password,
        password_confirmation: data.password_confirmation,
      })
      .then((res) => {
        if (res.data.success === 1) {
          navigate(route);
          dispatch.count.setToken(res.data.data.token);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className={"mb-12"}>
        <Controller
          name={"name"}
          control={control}
          render={({ field }) => (
            <TextField
              fullWidth
              sx={{ marginBottom: 3 }}
              placeholder={"Inserisci il tuo nome"}
              label={"Nome"}
              variant={"outlined"}
              {...field}
            />
          )}
        />
        {errors.name && (
          <p className={"text-red-500 mb-4"}>{errors.name.message}</p>
        )}
        <Controller
          name={"lastname"}
          control={control}
          render={({ field }) => (
            <TextField
              sx={{ marginBottom: 3 }}
              fullWidth
              placeholder={"Inserisci il tuo cognome"}
              label={"Cognome"}
              variant={"outlined"}
              {...field}
            />
          )}
        />
        {errors.lastname && (
          <p className={"text-red-500  mb-4"}>{errors.lastname.message}</p>
        )}
        <Controller
          name={"email"}
          control={control}
          render={({ field }) => (
            <TextField
              fullWidth
              sx={{ marginBottom: 3 }}
              placeholder={"Inserisci la tua email"}
              label={"Email"}
              variant={"outlined"}
              {...field}
            />
          )}
        />
        {errors.email && (
          <p className={"text-red-500  mb-4"}>{errors.email.message}</p>
        )}
        <Controller
          name={"password"}
          control={control}
          render={({ field }) => (
            <TextField
              fullWidth
              sx={{ marginBottom: 3 }}
              placeholder={"Inserisci la tua password"}
              label={"Password"}
              type={"password"}
              variant={"outlined"}
              {...field}
            />
          )}
        />
        {errors.password && (
          <p className={"text-red-500  mb-4"}>{errors.password.message}</p>
        )}
        <Controller
          name={"password_confirmation"}
          control={control}
          render={({ field }) => (
            <TextField
              fullWidth
              sx={{ marginBottom: 3 }}
              placeholder={"Conferma la password"}
              label={"Conferma Password"}
              type={"password"}
              variant={"outlined"}
              {...field}
            />
          )}
        />
        {errors.password_confirmation && (
          <p className={"text-red-500  mb-4"}>
            {errors.password_confirmation.message}
          </p>
        )}
        <Button
          size={"large"}
          className={"w-full"}
          variant={"contained"}
          type="submit"
        >
          REGISTRATI
        </Button>
      </div>
    </form>
  );
};

export default RegisterCustom;
