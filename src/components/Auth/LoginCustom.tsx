import React from "react";
import { Controller, useForm } from "react-hook-form";
import { Button, TextField } from "@mui/material";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Dispatch } from "../../rematch/store";

interface IFormInputs {
  email: string;
  password: string;
}

interface LoginCustomProps {
  url: string;
  route: string;
  setLoading: (b: boolean) => void;
}

const SignupSchema = yup
  .object({
    email: yup.string().email().required("Campo obbligatorio"),
    password: yup.string().defined().required("Campo obbligatorio"),
  })
  .required();

const LoginCustom: React.FC<LoginCustomProps> = ({
  url,
  route,
  setLoading,
}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch<Dispatch>();
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<IFormInputs>({
    defaultValues: {
      email: "admin@gmail.com",
      password: "123456",
    },
    resolver: yupResolver(SignupSchema),
  });

  const onSubmit = (data: IFormInputs) => {
    setLoading(true);
    axios
      .post(url, { email: data.email, password: data.password })
      .then((res) => {
        if (res.data.success === 1) {
          var name = res.data.data.user.name.charAt(0).toUpperCase();
          var lastname = res.data.data.user.lastname.charAt(0).toUpperCase();
          dispatch.count.setName(res.data.data.user.name);
          dispatch.count.setLastName(res.data.data.user.lastname);
          dispatch.count.setIdUser(res.data.data.user.id);
          dispatch.count.setBedgeLetter(name + lastname);
          dispatch.count.setToken(res.data.data.token);
          navigate(route);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div className={"flex justify-center"}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className={"mb-12"}>
          <Controller
            name={"email"}
            control={control}
            render={({ field }) => (
              <TextField
                placeholder={"Inserisci la tua email"}
                label={"Email"}
                variant={"outlined"}
                {...field}
              />
            )}
          />
          {errors.email && (
            <p className={"text-red-500"}>{errors.email.message}</p>
          )}
        </div>
        <div className={"mb-12"}>
          <Controller
            name={"password"}
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField
                placeholder={"Inserisci la tua password"}
                label={"Password"}
                type={"password"}
                variant={"outlined"}
                {...field}
              />
            )}
          />
          {errors.password && (
            <p className={"text-red-500"}>{errors.password.message}</p>
          )}
        </div>
        <Button className={"w-full"} variant={"contained"} type="submit">
          LOGIN
        </Button>
      </form>
    </div>
  );
};

export default LoginCustom;
