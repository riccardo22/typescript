import React from "react";
import { Routes, Route } from "react-router-dom";
import Page1 from "./Screen/Page1/page1";
import Page2 from "./Screen/Page2/Page2";
import Page3 from "./Screen/Page3/page3";
import Login from "./Screen/Auth/login";
import Registrazione from "./Screen/Auth/registrazione";
import { Provider } from "react-redux";
import { store } from "./rematch/store";
import Home from "./Screen/Home/home";

function App() {
  return (
    <Provider store={store}>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/home" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Registrazione />} />
        <Route path="/page1" element={<Page1 />} />
        <Route path="/page2" element={<Page2 />} />
        <Route path="/page3" element={<Page3 />} />
      </Routes>
    </Provider>
  );
}

export default App;
