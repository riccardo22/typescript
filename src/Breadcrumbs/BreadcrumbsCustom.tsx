import * as React from "react";
import WhatshotIcon from "@mui/icons-material/Whatshot";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Link from "@mui/material/Link";
import HomeIcon from "@mui/icons-material/Home";
import { useNavigate } from "react-router-dom";
import LoginIcon from "@mui/icons-material/Login";
import ShoppingBasketIcon from "@mui/icons-material/ShoppingBasket";
import { useLocation } from "react-router-dom";

const BreadcrumbsCustom = () => {
  const navigate = useNavigate();
  const location = useLocation();
  return (
    <div role="presentation" className={"mt-10 ml-12"}>
      <Breadcrumbs aria-label="breadcrumb">
        <Link
          onClick={() => navigate("/login")}
          underline="hover"
          sx={{ display: "flex", alignItems: "center", cursor: "pointer" }}
          color={"inherit"}
          variant={"body2"}
        >
          <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
          Login
        </Link>
        <Link
          onClick={() => navigate("/register")}
          underline="hover"
          sx={{ display: "flex", alignItems: "center", cursor: "pointer" }}
          color={"inherit"}
          variant={"body2"}
        >
          <LoginIcon sx={{ mr: 0.5 }} fontSize="inherit" />
          Registrazione
        </Link>
        <Link
          onClick={() => navigate("/home")}
          underline="hover"
          sx={{ display: "flex", alignItems: "center", cursor: "pointer" }}
          variant={"body2"}
          color={location.pathname !== "/home" ? "inherit" : "primary"}
        >
          <ShoppingBasketIcon sx={{ mr: 0.5 }} fontSize="inherit" />
          Shop
        </Link>
        <Link
          onClick={() => navigate("/page1")}
          underline="hover"
          sx={{ display: "flex", alignItems: "center", cursor: "pointer" }}
          variant={"body2"}
          color={location.pathname !== "/page1" ? "inherit" : "primary"}
        >
          <WhatshotIcon sx={{ mr: 0.5 }} fontSize="inherit" />
          Page1
        </Link>
      </Breadcrumbs>
    </div>
  );
};

export default BreadcrumbsCustom;
