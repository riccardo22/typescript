export interface ListCustomProps {
  id: number;
  name: string;
  lastname: string;
  email: string;
}

export interface ItemProps {
  item: ListCustomProps[];
}
