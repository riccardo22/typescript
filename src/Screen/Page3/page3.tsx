import React, { useState } from "react";
import Layout from "../../components/Layout/Layout";
import TabsNavigationCustom from "../../components/TabsNavigation/TabsNavigationCustom";
import { useSelector } from "react-redux";
import { RootState } from "../../rematch/store";
import axios from "axios";
import useSWR from "swr";
import { url } from "../../conf/env.json";
import LoadingCustom from "../../components/Loading/LoadingCustom";
import GridMap from "../../components/GridItemMap/GridMap";
import netflix from "../../assets/img/netflix.webp";
import prime from "../../assets/img/prime.webp";
import disney from "../../assets/img/disney.webp";
import mediaset from "../../assets/img/mediaset.webp";
import infinity from "../../assets/img/infinity.webp";
import rai from "../../assets/img/rai.webp";
import Streaming from "../../components/Streaming/Straming";

const all = "https://picsum.photos/200/200";

const streaming = [
  {
    id: 0,
    img: all,
  },
  {
    id: 1,
    img: netflix,
  },
  {
    id: 2,
    img: prime,
  },
  {
    id: 3,
    img: disney,
  },
  {
    id: 4,
    img: mediaset,
  },
  {
    id: 5,
    img: infinity,
  },
  {
    id: 6,
    img: rai,
  },
];

const Page3 = () => {
  const count = useSelector((state: RootState) => state.count);
  const [pageMovies, setPageMovies] = useState<number>(1);
  const [pageSeries, setPageSeries] = useState<number>(1);
  const [Idstreaming, setIdStreaming] = useState<number>(0);

  const axiosConfig = {
    headers: {
      Authorization: "Bearer " + count.token,
    },
  };
  const fetcher = (url: string) =>
    axios
      .post(url, { page: pageMovies, streaming: Idstreaming }, axiosConfig)
      .then((res) => res.data.data);
  const { data: movies, error: errorMovies } = useSWR(
    [`${url}/movies`, count.token, pageMovies, Idstreaming],
    fetcher
  );

  const fetcher1 = (url: string) =>
    axios
      .post(url, { page: pageSeries, streaming: Idstreaming }, axiosConfig)
      .then((res) => res.data.data);
  const { data: series, error: errorSeries } = useSWR(
    [`${url}/series`, count.token, pageSeries, Idstreaming],
    fetcher1
  );

  if (errorMovies || errorSeries) return <div>failed to load</div>;
  if (!movies || !series) return <LoadingCustom />;
  return (
    <Layout>
      <div
        className={
          "grid grid-cols-7 flex-col w-1/5 mt-4 justify-items-center gap-2 ml-4"
        }
      >
        {streaming.map((res) => (
          <Streaming
            key={res.id}
            img={res.img}
            onClickCustom={() =>
              res.id === 0 ? setIdStreaming(0) : setIdStreaming(res.id)
            }
          />
        ))}
      </div>
      <TabsNavigationCustom
        children1={
          <GridMap
            title={"FILM"}
            data={movies.data}
            pageTotal={movies.last_page}
            setPage={setPageMovies}
          />
        }
        children2={
          <GridMap
            title={"SERIE TV"}
            data={series.data}
            pageTotal={series.last_page}
            setPage={setPageSeries}
          />
        }
      />
    </Layout>
  );
};

export default Page3;
