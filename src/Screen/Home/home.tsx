import * as React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../rematch/store";
import { memo, useState } from "react";
import Layout from "../../components/Layout/Layout";
import axios from "axios";
import useSWR from "swr";
import { url } from "../../conf/env.json";
import LoadingCustom from "../../components/Loading/LoadingCustom";
import ItemShop from "../../components/ItemShop/ItemShop";
import PaginationCustom from "../../components/Pagination/PaginationCustom";
import BreadcrumbsCustom from "../../Breadcrumbs/BreadcrumbsCustom";
import GridMap from "../../components/GridItemMap/GridMap";

export interface ProductsProps {
  id: number;
  img: string;
}

const Home: React.FC = () => {
  const count = useSelector((state: RootState) => state.count);
  const [page, setPage] = useState(1);
  const axiosConfig = {
    headers: {
      Authorization: "Bearer " + count.token,
    },
  };
  const fetcher = (url: string) =>
    axios.post(url, { page: page }, axiosConfig).then((res) => res.data.data);
  const { data, error } = useSWR([`${url}/movies`, count.token, page], fetcher);

  if (error) return <div>failed to load</div>;
  if (!data) return <LoadingCustom />;
  return (
    <Layout>
      <BreadcrumbsCustom />
      <GridMap
        title={"FILM"}
        data={data.data}
        pageTotal={data.last_page}
        setPage={setPage}
      />
    </Layout>
  );
};

export default memo(Home);
