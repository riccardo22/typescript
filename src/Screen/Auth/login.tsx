import React, { useState } from "react";
import LoginCustom from "../../components/Auth/LoginCustom";
import { Link, Typography } from "@mui/material";
import { url } from "../../conf/env.json";
import LoadingCustom from "../../components/Loading/LoadingCustom";

const logo = "https://source.unsplash.com/random/90x80";

const Login = () => {
  const [loading, setLoading] = useState<boolean>(false);
  return (
    <div className={"flex items-center justify-center h-screen flex-col"}>
      {loading ? (
        <LoadingCustom />
      ) : (
        <div
          className={"w-1/5 shadow-2xl p-8 rounded-2xl border border-gray-200"}
        >
          <img
            className={"mb-8 rounded-full  mx-auto"}
            alt={"logo"}
            src={logo}
          />
          <Typography
            variant={"h5"}
            sx={{ marginBottom: 3 }}
            textAlign={"center"}
          >
            Login
          </Typography>
          <LoginCustom
            url={`${url}/login`}
            route={"/home"}
            setLoading={setLoading}
          />
          <div className={"text-center mt-8"}>
            <Link href={"/register"}>Non hai un account? Registrati</Link>
          </div>
        </div>
      )}
    </div>
  );
};

export default Login;
