import React, { useState } from "react";
import { Link, Typography } from "@mui/material";
import RegisterCustom from "../../components/Auth/RegisterCustom";
import { url } from "../../conf/env.json";
import LoadingCustom from "../../components/Loading/LoadingCustom";

const logo = "https://source.unsplash.com/random/90x80";

const Registrazione = () => {
  const [loading, setLoading] = useState<boolean>(false);
  return (
    <div className={"flex items-center justify-center h-screen flex-col"}>
      {loading ? (
        <LoadingCustom />
      ) : (
        <div
          className={"w-1/4 shadow-2xl p-8 rounded-2xl border border-gray-200"}
        >
          <img
            className={"mb-8 rounded-full  mx-auto"}
            alt={"logo"}
            src={logo}
          />
          <Typography
            component={"h5"}
            variant={"h5"}
            sx={{ marginBottom: 3 }}
            textAlign={"center"}
          >
            Registrazione
          </Typography>
          <RegisterCustom
            url={`${url}/register`}
            route={"/home"}
            setLoading={setLoading}
          />
          <div className={"text-center mt-8"}>
            <Link href={"/login"}>Hai un account? Vai al login</Link>
          </div>
        </div>
      )}
    </div>
  );
};

export default Registrazione;
