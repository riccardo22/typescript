import React, { useEffect, useState } from "react";
import { Typography } from "@mui/material";
import Home from "../Home/home";
import Box from "@mui/material/Box";
import useSWR from "swr";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch, RootState } from "../../rematch/store";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import { url } from "../../conf/env.json";
import LoadingCustom from "../../components/Loading/LoadingCustom";
import { PaginationCustom } from "../../components/Paginazione/PaginationCustom";
import { makeStyles } from "@mui/styles";
import DeleteIcon from "@mui/icons-material/Delete";
import Button from "@mui/material/Button";
import EditIcon from "@mui/icons-material/Edit";
import AddIcon from "@mui/icons-material/Add";
import ModalCustom from "../../components/Modal/ModalCustom";
import FormikModal from "../../components/Formik/FormikModal";
import SnackbarsCustom from "../../components/Snackbar/SnackbarCustom";
import DialogCustom from "../../components/Dialog/DialogCustom";
import Layout from "../../components/Layout/Layout";
import BreadcrumbsCustom from "../../Breadcrumbs/BreadcrumbsCustom";

const Page1 = () => {
  const classes = useStyles();

  const [openModalAdd, setOpenModelAdd] = useState(false);
  const [openModalEdit, setOpenModelEdit] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const [idRow, setIdRow] = useState();

  const count = useSelector((state: RootState) => state.count);
  const dispatch = useDispatch<Dispatch>();
  const axiosConfig = {
    headers: {
      Authorization: "Bearer " + count.token,
    },
  };
  const fetcher = (url: string) =>
    axios
      .post(url, { id: count.idUser }, axiosConfig)
      .then((res) => res.data.data);
  const { data, error } = useSWR(
    [`${url}/customers`, count.token, count.snackbar],
    fetcher
  );
  const fetcher1 = (url: string) =>
    axios.post(url, { id: idRow }, axiosConfig).then((res) => res.data.data);
  const { data: customer, error: errorCustomer } = useSWR(
    [`${url}/get-customer`, idRow],
    fetcher1
  );

  useEffect(() => {
    count.snackbar === true && setOpenSnackbar(true);
    dispatch.count.setSnackbar(false);
  }, [count.snackbar]);

  const columns: GridColDef[] = [
    { field: "name", headerName: "Nome", flex: 1 },
    { field: "lastname", headerName: "Cognome", flex: 1 },
    { field: "email", headerName: "Email", flex: 1 },
    {
      field: "actions",
      type: "actions",
      flex: 1,
      renderCell: (params) => {
        return (
          <>
            <Button
              onClick={() => {
                setIdRow(params.row.id);
                setOpenModelEdit(true);
              }}
              startIcon={<EditIcon />}
              sx={{ marginRight: 5 }}
              variant={"contained"}
            >
              MODIFICA
            </Button>
            <Button
              color={"error"}
              onClick={() => {
                setIdRow(params.row.id);
                setOpenDialog(true);
              }}
              startIcon={<DeleteIcon />}
              variant={"contained"}
            >
              ELIMINA
            </Button>
          </>
        );
      },
    },
  ];

  const onClickButtonDelete = () => {
    axios
      .post(`${url}/delete-customers`, { id: idRow }, axiosConfig)
      .then((res) => {
        if (res.data.success === 1) {
          dispatch.count.setSnackbar(true);
          setOpenDialog(false);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  if (error) return <div>failed to load</div>;
  if (!data) return <LoadingCustom />;
  return (
    <Layout>
      <SnackbarsCustom open={openSnackbar} setOpen={setOpenSnackbar} />
      <BreadcrumbsCustom />
      <Typography variant={"h4"} sx={{ marginTop: 2 }} align={"center"}>
        Contatti
      </Typography>
      <div className={"flex justify-end mr-6"}>
        <Button
          onClick={() => setOpenModelAdd(true)}
          size={"large"}
          color={"success"}
          startIcon={<AddIcon />}
          variant={"contained"}
        >
          AGGIUNGI
        </Button>

        <ModalCustom open={openModalAdd} setOpen={setOpenModelAdd}>
          <FormikModal
            title={"Aggiungi Cliente"}
            titleButton={"AGGIUNGI"}
            url={`${url}/add-customers`}
            setOpen={setOpenModelAdd}
          />
        </ModalCustom>
        <ModalCustom open={openModalEdit} setOpen={setOpenModelEdit}>
          <FormikModal
            title={"Modifica Cliente"}
            titleButton={"MODIFICA"}
            url={`${url}/add-customers`}
            setOpen={setOpenModelEdit}
            customer={customer}
          />
        </ModalCustom>
      </div>
      <DialogCustom
        open={openDialog}
        setOpen={setOpenDialog}
        content={"Sicuro di voler eliminare questo contatto?"}
        titleButton={"ELIMINA"}
        onClickButton={onClickButtonDelete}
        title={"Attenzione!"}
        colorButton={"error"}
      />
      <Box className={"w-full flex justify-center "}>
        <Box
          className={
            "w-full justify-items-start grid grid-cols-1 border border-2 border-gray-300 rounded-lg m-4 shadow-2xl"
          }
        >
          <div style={{ height: "100vh", width: "100%" }}>
            <DataGrid
              disableColumnMenu
              hideFooterSelectedRowCount
              pageSize={15}
              className={classes.root}
              rows={data}
              columns={columns}
              components={{
                Pagination: PaginationCustom,
              }}
            />
          </div>
        </Box>
      </Box>
    </Layout>
  );
};

export default Page1;

const useStyles = makeStyles({
  root: {
    "&.MuiDataGrid-root .MuiDataGrid-cell:focus-within": {
      outline: "none",
    },
  },
});
