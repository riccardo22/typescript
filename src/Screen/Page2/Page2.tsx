import React from "react";
import Layout from "../../components/Layout/Layout";
import SliderCustom from "../../components/Slider/SliderCustom";
import axios from "axios";
import useSWR from "swr";
import { url } from "../../conf/env.json";
import LoadingCustom from "../../components/Loading/LoadingCustom";
import { useSelector } from "react-redux";
import { RootState } from "../../rematch/store";

const Page2 = () => {
  const count = useSelector((state: RootState) => state.count);
  const axiosConfig = {
    headers: {
      Authorization: "Bearer " + count.token,
    },
  };

  const fetcher = (url: string) =>
    axios.post(url, { page: 1 }, axiosConfig).then((res) => res.data.data.data);
  const { data: movies, error: errorMovies } = useSWR(
    [`${url}/movies`, count.token],
    fetcher
  );

  const fetcher2 = (url: string) =>
    axios.post(url, { page: 1 }, axiosConfig).then((res) => res.data.data.data);
  const { data: series, error: errorSeries } = useSWR(
    [`${url}/series`, count.token],
    fetcher2
  );

  const fetcher3 = (url: string) =>
    axios.get(url, axiosConfig).then((res) => res.data.data);
  const { data: all, error: errorAll } = useSWR(
    [`${url}/all`, count.token],
    fetcher3
  );

  if (errorMovies || errorSeries || errorAll) return <div>failed to load</div>;
  if (!movies || !series || !all) return <LoadingCustom />;
  return (
    <Layout>
      <SliderCustom data={all} title={"NEW"} />
      <SliderCustom data={movies} title={"FILM"} />
      <SliderCustom data={series} title={"SERIE TV"} />
    </Layout>
  );
};

export default Page2;
