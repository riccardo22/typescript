import { init, RematchDispatch, RematchRootState } from "@rematch/core";
import { models, RootModel } from "./models";
import createRematchPersist from "@rematch/persist";
import AsyncStorage from "@react-native-async-storage/async-storage";

const persistPlugin = createRematchPersist<
  RematchRootState<RootModel>,
  RootModel
>({
  key: "root",
  storage: AsyncStorage,
});

export const store = init<RootModel>({
  models,
  plugins: [persistPlugin],
});

export type Store = typeof store;
export type Dispatch = RematchDispatch<RootModel>;
export type RootState = RematchRootState<RootModel>;
