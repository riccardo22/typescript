import { createModel } from "@rematch/core";
import { RootModel } from "./models";

export const count = createModel<RootModel>()({
  state: {
    token: 0,
    idUser: 0,
    name: "",
    lastname: "",
    bedgeLetter: "",
    snackbar: false,
    cart: 0,
  },
  reducers: {
    setToken(state, token: number) {
      return {
        ...state,
        token,
      };
    },
    setIdUser(state, idUser: number) {
      return {
        ...state,
        idUser,
      };
    },
    setName(state, name: string) {
      return {
        ...state,
        name,
      };
    },
    setLastName(state, lastname: string) {
      return {
        ...state,
        lastname,
      };
    },
    setBedgeLetter(state, bedgeLetter: string) {
      return {
        ...state,
        bedgeLetter,
      };
    },
    setSnackbar(state, snackbar: boolean) {
      return {
        ...state,
        snackbar,
      };
    },
    setCart(state, cart: number) {
      return {
        ...state,
        cart,
      };
    },
  },
});
